package com.zyplayer.doc.manage.web;

import com.zyplayer.doc.core.json.DocResponseJson;
import com.zyplayer.doc.core.json.ResponseJson;
import com.zyplayer.doc.git.service.GitServiceImpl;
import com.zyplayer.doc.manage.framework.SchedulerTask.HtmlConvertToMdSchedulerTask;
import lombok.RequiredArgsConstructor;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.StatusCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/git")
@RequiredArgsConstructor
public class GitManageFileController {

    private final GitServiceImpl gitService;

    private final HtmlConvertToMdSchedulerTask htmlConvertToMd;


    @GetMapping("/clone")
    public ResponseJson<Object> gitClone() {
        Git git = null;
        try {
            git = gitService.gitClone();
        } catch (GitAPIException e) {
            throw new RuntimeException(e);
        }
        return DocResponseJson.ok(git);
    }

    @GetMapping("/push")
    public void gitPush() {
        try {
            gitService.oneClickPush("测试");
        } catch (GitAPIException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/pull")
    public void gitPull() {
        try {
            gitService.gitPull();
        } catch (GitAPIException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/gitInfo")
    public StatusCommand getGitInfo() {
        Git git = gitService.getGit();
        return git.status();
    }


    @GetMapping("/generatorMdTask")
    public void htmlConvertToMd() {
        htmlConvertToMd.generatorMdTask();
    }
}
