package com.zyplayer.doc.git.utils;

import lombok.Getter;

/**
 * 服务器所属的操作系统信息类
 **/
public class OsInfo {
    /**
     * -- GETTER --
     * 获取操作系统名字
     *
     * @return 操作系统名
     */
    @Getter
    private static final String osName = System.getProperty("os.name").toLowerCase();

    @Getter
    private static final String version = System.getProperty("os.version");

    @Getter
    private static final String arch = System.getProperty("os.arch");

    public static boolean isLinux() {
        return osName.contains("linux");
    }

    public static boolean isMacosName() {
        return osName.contains("mac") && osName.indexOf("os") > 0 && !osName.contains("x");
    }

    public static boolean isMacosNameX() {
        return osName.contains("mac") && osName.indexOf("os") > 0 && osName.indexOf("x") > 0;
    }

    public static boolean isWindows() {
        return osName.contains("windows");
    }

    public static boolean isosName2() {
        return osName.contains("os/2");
    }

    public static boolean isSolaris() {
        return osName.contains("solaris");
    }

    public static boolean isSunosName() {
        return osName.contains("sunos");
    }

    public static boolean isMPEiX() {
        return osName.contains("mpe/ix");
    }

    public static boolean isHPUX() {
        return osName.contains("hp-ux");
    }

    public static boolean isAix() {
        return osName.contains("aix");
    }

    public static boolean isosName390() {
        return osName.contains("os/390");
    }

    public static boolean isFreeBSD() {
        return osName.contains("freebsd");
    }

    public static boolean isIrix() {
        return osName.contains("irix");
    }

    public static boolean isDigitalUnix() {
        return osName.contains("digital") && osName.indexOf("unix") > 0;
    }

    public static boolean isNetWare() {
        return osName.contains("netware");
    }

    public static boolean isosNameF1() {
        return osName.contains("osf1");
    }

    public static boolean isOpenVMS() {
        return osName.contains("openvms");
    }

}
