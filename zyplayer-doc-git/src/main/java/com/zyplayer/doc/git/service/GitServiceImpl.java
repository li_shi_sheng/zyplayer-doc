package com.zyplayer.doc.git.service;


import lombok.RequiredArgsConstructor;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GitServiceImpl extends GitService {
    @Override
    public Git gitClone() throws GitAPIException {
        return super.gitClone();
    }

    @Override
    public PullResult gitPull() throws GitAPIException {
        return super.gitPull();
    }

    @Override
    public void oneClickPush(String commitMsg) throws GitAPIException {
        super.oneClickPush(commitMsg);
    }
}
